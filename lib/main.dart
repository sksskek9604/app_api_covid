import 'package:app_api_covid/pages/page_index.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'hospital of covid',
      theme: ThemeData(

        primarySwatch: Colors.purple,
      ),
      home: const PageIndex(),
    );
  }
}
