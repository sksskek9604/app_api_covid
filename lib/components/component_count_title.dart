import 'package:flutter/material.dart';

// 카운트 타이틀
class ComponentCountTitle extends StatelessWidget {
  const ComponentCountTitle({super.key, required this.icon, required this.count, required this.unitName, required this.itemName});

  final IconData icon;
  final int count;
  final String unitName;
  final String itemName;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(icon, size: 30,color: Colors.yellow.shade700,),
            const SizedBox(width: 10),
            Text('총 ${count.toString()}${unitName}의 ${itemName}이(가) 있습니다.', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white70),),
          ],
        ),
      )
    );
  }
}
