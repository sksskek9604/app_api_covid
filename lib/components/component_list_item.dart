import 'package:app_api_covid/model/hospital_list_item.dart';
import 'package:flutter/material.dart';

// 정돈된 리스트를 위한 컴포넌츠

class ComponentListItem extends StatelessWidget {
  const ComponentListItem({super.key, required this.item, required this.callback});

  final HospitalListItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(30),
        margin: const EdgeInsets.only(top: 25, left: 20, right: 30, bottom: 25),
        decoration: BoxDecoration(
          color: Colors.white54,
          border: Border(
            left: BorderSide(color: Colors.purple.shade800, width: 10,)
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text( '기관코드: ' + '${item.orgcd == null ? '기관코드 정보가 없습니다. ' : item.orgcd}', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            Text('병원 이름: ' + '${item.orgnm == null ? '병원 이름 정보가 없습니다. ' : item.orgnm}', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.purple.shade800),),
            Text('연락처: ' + '${item.orgTlno == null ? '기관 전화번호 정보가 없습니다. ' : item.orgTlno}', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
            Text('주소: ' + '${item.orgZipaddr == null ? '기관주소 정보가 없습니다. ' : item.orgZipaddr}', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
            Text('현재 기준일: ' + '${item.slrYmd == null ? '현재 기준일자가 없습니다. ' : item.slrYmd}', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
            Text('현재 기준일(요일): ' + '${item.dywk == null ? '현재 기준일자 요일 정보가 없습니다 ' : item.dywk}', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
            Text('당일 휴무(Y/N): ' + '${item.hldyYn == null ? '(당일기준) 휴무 여부 정보가 없습니다 ' : item.hldyYn}', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600, color: Colors.red.shade300),),
            Text('점심시작: ' + '${item.lunchSttTm == null ? '(당일기준) 점심시작 시간 정보가 없습니다. ' : item.lunchSttTm}', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
            Text('점심종료: ' + '${item.lunchEndTm == null ? '(당일기준) 점심종료 시간 정보가 없습니다. ' : item.lunchEndTm}', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
            Text('진료시작: ' + '${item.sttTm == null ? '(당일기준) 진료시작 시간 정보가 없습니다.' : item.sttTm}', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
            Text('진료종료: ' + '${item.endTm == null ? '(당일기준) 진료종료 시간 정보가 없습니다. ' : item.endTm}', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
          ],
        ),
      ),
    );
  }
}
