import 'package:app_api_covid/model/hospital_list_item.dart';
import 'package:flutter/material.dart';

class HospitalList {
  int page;
  int perPage;
  int totalCount;
  int currentCount;
  int matchCount;
  List<HospitalListItem>? data; //nullable 필드, 만약 data2가 생기면 중괄호안에 넣어주면 됨.

  HospitalList(
      this.page,
      this.perPage,
      this.totalCount,
      this.currentCount,
      this.matchCount,
      {this.data});

  factory HospitalList.fromJson(Map<String, dynamic> json ) {
    return HospitalList(
      json['page'],
      json['perPage'],
      json['totalCount'],
      json['currentCount'],
      json['matchCount'],
      data: json['data'] != null ? (json['data'] as List).map((e) => HospitalListItem.fromJson(e)).toList() : null,
      // 람다식 삼항연산자 값이 있으면 ~.toList()까지 처리하고 , 없으면 null로 처리한다.
    );
  }
}