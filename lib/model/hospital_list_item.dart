import 'package:flutter/material.dart';

class HospitalListItem {
  String? orgcd; // 기관코드
  String? orgnm; // 기관명
  String? orgTlno; // 기관 전화번호
  String? orgZipaddr; // 기관주소
  String? slrYmd; // 기준일자(현재날짜)
  String? dywk; // 기준일자 요일
  String? hldyYn; // 기준일자 휴무일 여부
  String? lunchSttTm; // 기준일자 점심시작 시간
  String? lunchEndTm; // 기준일자 점심종료 시간
  String? sttTm; // 기준일자 진료시작 시간
  String? endTm; // 기준일자 진료종료 시간

  HospitalListItem(
      {this.orgcd,
        this.orgnm,
        this.orgTlno,
        this.orgZipaddr,
        this.slrYmd,
        this.dywk,
        this.hldyYn,
        this.lunchSttTm,
        this.lunchEndTm,
        this.sttTm,
        this.endTm});

  factory HospitalListItem.fromJson(Map<String, dynamic> json) {
    return HospitalListItem(
      orgcd: json['orgcd'] != null ? json['orgcd'] : null,
      // orgcd : json'orgcd'가 null이 아니면? json'orgcd'이고 , 그게 아니면 null을 반환한다
      orgnm: json['orgnm'] != null ? json['orgnm'] : null,
      orgTlno: json['orgTlno'] != null ? json['orgTlno'] : null,
      orgZipaddr: json['orgZipaddr'] != null ? json['orgZipaddr'] : null,
      slrYmd: json['slrYmd'] != null ? json['slrYmd'] : null,
      dywk: json['dywk'] != null ? json['dywk'] : null,
      hldyYn: json['hldyYn'] != null ? json['hldyYn'] : null,
      lunchSttTm: json['lunchSttTm'] != null ? json['lunchSttTm'] : null,
      lunchEndTm: json['lunchEndTm'] != null ? json['lunchEndTm'] : null,
      sttTm: json['sttTm'] != null ? json['sttTm'] : null,
      endTm: json['endTm'] != null ? json['endTm'] : null,
    );
  }
}