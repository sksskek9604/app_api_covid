import 'package:app_api_covid/components/component_count_title.dart';
import 'package:app_api_covid/components/component_list_item.dart';
import 'package:app_api_covid/components/component_no_contents.dart';
import 'package:app_api_covid/model/hospital_list_item.dart';
import 'package:app_api_covid/repository/repo_hospital_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  // 무한 스크롤
  final _scrollController = ScrollController();
  final _formKey = GlobalKey<FormBuilderState>();
  bool _areaHasError = false;
  var areaOptions = ['전체', '경기도 안산시', '서울특별시', '경기도 수원시'];


  List<HospitalListItem> _list = [];
  int _page = 1; // 현재페이지
  int _totalPage = 1;  // 총 페이지 개수가 없어서 추가해주었음. (검색결과 개수에서 perPage로 나누고, 올림 ex: 567개면 56페이지? 57페이지?)
  int _perPage = 10; // 한 페이지당 보여줄 아이템 개수
  int _totalCount = 0; // 총 아이템 개수
  int _currentCount = 0; // 현재 보여지고 있는 아이템의 번호
  int _matchCount = 0; // 검색 결과 개수
  String _searchArea = '전체';

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _loadItems();
      } // 스크롤의 위치가 맨 아래쪽에 닿으면 _loadItems를 실행해 주세요.
    });
    _loadItems();
  }

  Future<void> _loadItems({bool reFresh = false}) async {
    if (reFresh) {
      _list = [];
      _page = 1;
      _totalPage = 1;
      _perPage = 10;
      _totalCount = 0;
      _currentCount = 0;
      _matchCount = 0;
    } // 위 아래 if위치 잘 확인, 비우고 -> 다시 가져오는 것

    if (_page <= _totalPage) {
      // 5. 이 조건에서만 실행이 되어야하니까 if 안으로 넣어준다.
      await RepoHospitalList()
          .getList(page: _page, searchArea: _searchArea) //2. 바닥에 닿으면 현재페이지를 1씩 증가시킨다.
          .then((res) => {
                setState(() {
                  _totalPage = (res.matchCount / res.perPage)
                      .ceil(); // 3. 이 검색결과에서 ceil = 올림
                  _totalCount = res.totalCount;
                  _currentCount = res.currentCount;
                  _matchCount = res.matchCount;

                  _list = [..._list, ...?res.data]; // 4. 배열의 길이가 계속해서 늘어나게 되는 것

                  if (res.data != null) _list = res.data!; // 꼭 가져와!

                  _page++; // 2. 바닥에 닿으면 현재페이지를 1씩 증가시킨다.
                })
              }) // 1. 성공하면(then), 가져온 뭉텅이를 res라고 부를거고, (result의 약자 res)
          .catchError((err) => {debugPrint(err)});
    }
    if (reFresh) {
      _scrollController.animateTo(0, duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              opacity: 0.25,
              fit: BoxFit.cover,
              image: AssetImage('assets/covid.jpg')
            )
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              title: const Text('코로나19 예방접종 병원 검색'),
            ),
            body: ListView(
              controller: _scrollController,
              children: [
                _buildBody(),
              ],
            ),
            bottomNavigationBar: BottomAppBar(
              shape: CircularNotchedRectangle(),
              child: FormBuilder(
                key: _formKey,
                autovalidateMode: AutovalidateMode.disabled,
                child: FormBuilderDropdown<String>(
                  // autovalidate: true,
                  name: 'area',
                  decoration: const InputDecoration(
                    labelText: '지역',
                    hintText: '지역 선택',
                  ),
                  items: areaOptions
                      .map((area) => DropdownMenuItem(
                    alignment: AlignmentDirectional.center,
                    value: area, // 전체라는 값으로 들어가니까, 이후에 맵으로 다시 수정
                    child: Text(area),
                  ))
                      .toList(),
                  onChanged: (val) {
                    // 값이 변경됐을 때 무엇을 할건지?
                    // => 기존의 리스트를 비우고, 검색어를 넣고 다시 리스트를 가져오겠다.
                    // => 모든 값이 초기화가 먼저 되고, 검색어가 들어감
                    setState(() {
                      _searchArea =
                          _formKey.currentState!.fields['area']!.value; // 필수 느낌표
                      _loadItems(reFresh: true);
                    });
                  },
                  valueTransformer: (val) => val?.toString(),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildBody() {
    // _matchCount(결과 데이터)가 있으면 SingleChildScrollView로 보여준다. (but, ListView일 경우는 필요없음!)
    if (_matchCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ComponentCountTitle(
            icon: Icons.add_alert,
            unitName: '건',
            itemName: '병원',
            count: _matchCount,
          ),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length, //현재 가진 리스트의 개수만큼만 반복
              itemBuilder: (_, index) => ComponentListItem(
                item: _list[index],
                callback: () {},
              ))
        ],
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 50,
        // 사이즈 박스의 높이가 통채로 - appBar사이즈(50) 빼준 영역
        child: const ComponentNoContents(icon: Icons.add_alert, msg: '데이터가 없습니다.'),
      );
    }
  }
}
