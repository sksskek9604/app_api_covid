import 'package:app_api_covid/config/config_api.dart';
import 'package:app_api_covid/model/hospital_list.dart';
import 'package:dio/dio.dart';

class RepoHospitalList { //api에 가서 데이터를 가져오는 레포지터리
  //1. 어디로 가야하는지 endPoint지정
  final String _baseUrl = 'https://api.odcloud.kr/api/apnmOrg/v1/list?page={page}&perPage={perPage}&serviceKey=ffzbt251LaJ%2F7rHLJrCbVET2d8hg7FDVqSHWJ1HtP9TwtJF%2Fyr20ixpa73bbbQwgZYp8br1jyNhYF9aUc6rEGQ%3D%3D';

  Future<HospitalList> getList({ int page = 1, int perPage = 10, String searchArea = '전체'}) async {
    String _resultUrl = _baseUrl.replaceAll('{page}', page.toString());
    _resultUrl = _resultUrl.replaceAll('{page}',page.toString());
    if (searchArea != '전체') {
      _resultUrl = _resultUrl + '&cond%5BorgZipaddr%3A%3ALIKE%5D=${Uri.encodeFull(searchArea)}'; // 인코딩할때의 인코드
    }

    Dio dio = Dio();

    final response = await dio.get(
        _resultUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              if (status == 200) {
                return true;
              } else {
                return false;
              }
            }
        )
    );
    // response가 갖고온 데이터(화면상의 data랑은 다름, 사실상 data.data라는 형태로 보여지는 것.. 이름을 다르게 해줘야함)
    return HospitalList.fromJson(response.data);
  }
}