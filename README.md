# app_api_covid

### * 공공 데이터 오픈 API이용
> * 활용 데이터 :
> >'공공데이터활용지원센터_코로나19 예방접종 위탁의료기관 조회서비스'

### 기능
> * 코로나19 예방접종 가능 병원 정보 리스트
> * 병원 지역 검색 필터

---

## Page_Index
> ##
> <img src="images/page_index.jpg" width="450" height="900">
>

## Filter_Search
> ##
> <img src="images/filter_search.jpg" width="450" height="900">
>

## Filter_Result
> ##
> <img src="images/filter_result.jpg" width="450" height="900">
>

